<?php
/**
 * @file includes for the accuweather module converted by RAP.
 */

// Load API
// todo: merge this with other load function
// todo: static caching

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_icon_def($icon = NULL) {
  $obj = array(
    '1' => array(
      'phrase' => 'Sunny',
      'icon_class' => 'sunny',
    ),
    '2' => array(
      'phrase' => 'Mostly Sunny',
      'icon_class' => 'sunny',
    ),
    '3' => array(
      'phrase' => 'Partly Sunny',
      'icon_class' => 'sunny',
    ),
    '4' => array(
      'phrase' => 'Intermittent Clouds',
      'icon_class' => 'cloudy',
    ),
    '5' => array(
      'phrase' => 'Hazy Sunshine',
      'icon_class' => 'partly-cloudy',
    ),
    '6' => array(
      'phrase' => 'Mostly Cloudy',
      'icon_class' => 'cloudy',
    ),
    '7' => array(
      'phrase' => 'Cloudy',
      'icon_class' => 'cloudy',
    ),
    '8' => array(
      'phrase' => 'Overcast',
      'icon_class' => 'cloudy',
    ),
    '11' => array(
      'phrase' => 'Fog',
      'icon_class' => 'cloudy',
    ),
    '12' => array(
      'phrase' => 'Showers',
      'icon_class' => 'rainy',
    ),
    '13' => array(
      'phrase' => 'Mostly Cloudy with Showers',
      'icon_class' => 'rainy',
    ),
    '14' => array(
      'phrase' => 'Partly Sunny with Showers',
      'icon_class' => 'rainy',
    ),
    '15' => array(
      'phrase' => 'Thunderstorms',
      'icon_class' => 'rainy',
    ),
    '16' => array(
      'phrase' => 'Mostly Cloudy with Thunderstorms',
      'icon_class' => 'rainy',
    ),
    '17' => array(
      'phrase' => 'Partly Sunny with Thunderstorms',
      'icon_class' => 'rainy',
    ),
    '18' => array(
      'phrase' => 'rainy',
      'icon_class' => 'rainy',
    ),
    '19' => array(
      'phrase' => 'Flurries',
      'icon_class' => 'rainy',
    ),
    '20' => array(
      'phrase' => 'Mostly Cloudy with Flurries',
      'icon_class' => 'rainy',
    ),
    '21' => array(
      'phrase' => 'Partly Sunny with Flurries',
      'icon_class' => 'rainy',
    ),
    '22' => array(
      'phrase' => 'Snow',
      'icon_class' => 'rainy',
    ),
    '23' => array(
      'phrase' => 'Mostly Cloudy with Snow',
      'icon_class' => 'rainy',
    ),
    '24' => array(
      'phrase' => 'Ice',
      'icon_class' => 'rainy',
    ),
    '25' => array(
      'phrase' => 'Sleet',
      'icon_class' => 'rainy',
    ),
    '26' => array(
      'phrase' => 'Freezing Rain',
      'icon_class' => 'rainy',
    ),
    '29' => array(
      'phrase' => 'Rain to Snow',
      'icon_class' => 'rainy',
    ),
    '30' => array(
      'phrase' => 'Hot',
      'icon_class' => 'sunny',
    ),
    '31' => array(
      'phrase' => 'Cold',
      'icon_class' => 'sunny',
    ),
    '32' => array(
      'phrase' => 'Windy',
      'icon_class' => 'sunny',
    ),
    '33' => array(
      'phrase' => 'Clear',
      'icon_class' => 'sunny',
    ),
    '34' => array(
      'phrase' => 'Mostly Clear',
      'icon_class' => 'sunny',
    ),
    '35' => array(
      'phrase' => 'Partly Cloudy',
      'icon_class' => 'partly-cloudy',
    ),
    '36' => array(
      'phrase' => 'Intermittent Clouds',
      'icon_class' => 'partly-cloudy',
    ),
    '37' => array(
      'phrase' => 'Hazy Moonlight',
      'icon_class' => 'partly-cloudy',
    ),
    '38' => array(
      'phrase' => 'Mostly Cloudy',
      'icon_class' => 'cloudy',
    ),
    '39' => array(
      'phrase' => 'Partly Cloudy with Showers',
      'icon_class' => 'rainy',
    ),
    '40' => array(
      'phrase' => 'Mostly Cloudy with Showers',
      'icon_class' => 'rainy',
    ),
    '41' => array(
      'phrase' => 'Partly Cloudy with Thunderstorms',
      'icon_class' => 'rainy',
    ),
    '42' => array(
      'phrase' => 'Mostly Cloudy with Thunderstorms',
      'icon_class' => 'rainy',
    ),
    '43' => array(
      'phrase' => 'Partly Cloudy with Flurries',
      'icon_class' => 'rainy',
    ),
    '44' => array(
      'phrase' => 'Partly Cloudy with Snow',
      'icon_class' => 'rainy',
    ),
  );

  if ($icon == NULL) {
    return $obj;
  }
  elseif (isset($obj[(int) $icon])) {
    return $obj[(int) $icon];
  }
  else {
    return NULL;
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_icon_to_phrase($icon) {
  $def = accuweather_icon_def($icon);

  if ($def) {
    return $def['phrase'];
  }
  else {
    return NULL;
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_icon_to_class($icon) {
  $def = accuweather_icon_def($icon);

  if ($def) {
    return $def['icon_class'];
  }
  else {
    return NULL;
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_location_to_tid($location) {
  $islands = array(
    'BAAB' => '50', // Abraham's Bay, Mayaguana 
    'MYBS' => '44', // Alice Town/S Bimini
    'BABP' => '42', // Behring Point, Anrdros
    'MYCG' => '43', // Church Grove, Berry Island
    'MYCT' => '49', // Clarence Town, Long Island 
    'BACK' => '51', // Cockbarn Town, Rum Cay/San Salvador
    'BACH' => '41', // Colonel Hill, Acklins/Crooked Island 
    'BACT' => '40', // Cooper's Town, The Abacos
    'MYDT' => '53', // Duncan Town (NOT IMPLEMENTED)
    'MYDU' => '46', // Dunmore/Harbour Isl, Eleuthra/Harbour Island 
    'MYGF' => '38', // Freeport, Grand Bahama
    'MYGR' => '40', // Green Turtle Cay, The Abacos
    'MYKB' => '42', // Kemp's Bay/Andros, Andros
    'BAMT' => '48', // Matthew Town, Inagua 
    'MYNN' => '37', // Nassau/Paradise Island
    'BAOB' => '45', // Old Bright, Cat Island
    'BAPN' => '52', // Port Nelson (NOT IMPLEMENTED)
    'BARS' => '46', // Rock Sound, Eleuthra/Harbour Island 
    'BASP' => '40', // Sandy Point, The Abacos
    'BASC' => '41', // Snug Corner, Acklins
    'MYBI' => '45', // The Bight/Cat Isl, Cat Island
    //'MYGW' => '', // West End, Grand Bahama Island
    'MYMH' => '40', // Marsh Harbour, The Abacos
    //'BA0006' => '', // Moore's Island (NOT IMPLEMENTED)
    //'PBI' => '', // Walker's Cay (NOT IMPLEMENTED)
    'BAGT' => '47', // George Town, The Exumas
    //'MYEG' => '', // Moss Town (NOT IMPLEMENTED)
    //'BA0004' => '', // Staniel Cay (NOT IMPLEMENTED)
  );

  $location = strtoupper($location);
  
  if (isset($islands[$location])) {
    return $islands[$location];
  }
  else {
    return NULL;
  }
}

// Convert API
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_convert($key, $value) {
  switch ($key) {
    case 'clean-url':
      return accuweather_convert_clean_url($value);
    case 'normalize-url':
      return str_replace('-', ' ', $value);
    case 'miles-to-kilometers':
      return round(($value * 1.6));
    case 'icon-to-phrase':
      $formatted_value = (int) $value;
      if ($phrase = accuweather_icon_to_phrase($formatted_value)) {
        return $phrase;
      }
      return $value;
    case 'icon-to-style':
      $formatted_value = (int) $value;
      if ($class = accuweather_icon_to_class($formatted_value)) {
        return $class;
      }
      return $value;
    default:
      return $value;
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_convert_clean_url($value) {
  $value = trim(strtolower($value));
  $value = str_replace(array(' ', '&', '/', '\\'), '-', $value);
  $value = str_replace(array('\''), '', $value);
  return $value;
}

// todo: implement returning a float
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_convert_temperature($value, $flag = 'c', $round = TRUE) {
  switch ($flag) {
    case 'f':
      return round(($value * 9 / 5) + 32);
    case 'c':
      return round(($value - 32) * 5 / 9);
  }
}

// Rebuild API
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_rebuild($op = 'all') {

  switch ($op) {
    case 'current':
      accuweather_rebuild_current();
      break;
    case 'five-day':
      accuweather_rebuild_five_day();
      break;
    default:
      // default to rebuilding all
      accuweather_rebuild_current();
      accuweather_rebuild_five_day();
  }

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_rebuild_current($force = FALSE) {

  drupal_load('module','framework');
  zend_framework_boot();

  include_once 'Zend/Http/Client.php';

  $xml_url = variable_get('accuweather_current_url', ACCUWEATHER_DEFAULT_CURRENT_URL);
  $user = variable_get('accuweather_login_user', ACCUWEATHER_DEFAULT_LOGIN_USER);
  $pass = variable_get('accuweather_login_pass', ACCUWEATHER_DEFAULT_LOGIN_PASS);

  try {
    $http = new Zend_Http_Client($xml_url);
    $http->setAuth($user, $pass);

    $response = $http->request();
    $feed = simplexml_load_string($response->getBody());

    $created = strtotime($feed->created);

    // clear the database table first
    db_truncate('accuweather_current')->execute();

    $insert = db_insert('accuweather_current')
                      ->fields(array(
      'aid',
      'created',
      'phrase',
      'temp_f',
      'temp_c',
      'aptemp_f',
      'aptemp_c',
      'wndchl_f',
      'wndchl_c',
      'rhumid',
      'wind_dir',
      'windspeed',
      'pres',
      'vis',
      'icon',
    ));

    // read through the xml and insert it into the db
    foreach ($feed->current as $item) {

      $location = trim($item->location); // note: gets rid of the weirdness

      $metadata = accuweather_get_metadata($location);

      if (!$metadata) {
        continue;
      }

      $insert->values(array(
        'aid' => (int) $metadata['aid'],
        'created' => $created, // todo: check based on created to avoid a db insert/update?
        'phrase' => accuweather_convert('icon-to-phrase', $item->icon),
        'temp_f' => $item->temp,
        'temp_c' => accuweather_convert_temperature($item->temp),
        'aptemp_f' => $item->aptemp,
        'aptemp_c' => accuweather_convert_temperature($item->aptemp),
        'wndchl_f' => $item->wndchl,
        'wndchl_c' => accuweather_convert_temperature($item->wndchl),
        'rhumid' => $item->rhumid,
        'wind_dir' => $item->wind_dir,
        'windspeed' => accuweather_convert('miles-to-kilometers', $item->windspeed),
        'pres' => $item->pres,
        'vis' => $item->vis,
        'icon' => $item->icon, //accuweather_convert('icon-to-style', $item->icon),
      ));
    }

    try {
      $insert->execute();
    }
    catch (Exception $e) {
      var_dump('CURRENT');
      var_dump($e);
    }

    watchdog('cron', 'Current Weather has been rebuilt.');

  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_rebuild_five_day($force = FALSE) {

  drupal_load('module','framework');
  zend_framework_boot();

  include_once 'Zend/Http/Client.php';

  // TODO: implement
  $xml_url = variable_get('accuweather_five_day_url', ACCUWEATHER_DEFAULT_FIVE_DAY_URL);
  $user    = variable_get('accuweather_login_user', ACCUWEATHER_DEFAULT_LOGIN_USER);
  $pass    = variable_get('accuweather_login_pass', ACCUWEATHER_DEFAULT_LOGIN_PASS);

  try {

    // using zend because http is so much better
    $http = new Zend_Http_Client($xml_url);
    $http->setAuth($user, $pass);
    $response = $http->request();
    $feed = simplexml_load_string(trim($response->getBody())); // trimming because it had some extra whitespace in the browser

    // clear the database table first
    db_truncate('accuweather_five_day')->execute();

    // read through the xml and insert it into the db
    foreach ($feed->forecast as $item) {

      // note: cleaning up dirty data
      $location = trim($item->citycode);

      $aid = accuweather_get_aid($location);

      if (!$aid) {
        continue;
      }

      $insert = db_insert('accuweather_five_day')
                          ->fields(array(
        'aid',
        'weight',
        'day',
        'phrase',
        'icon',
        'temp_high_f',
        'temp_high_c',
        'temp_low_f',
        'temp_low_c',
      ));

      $i = 0;
      foreach ($item->day as $day) {
        $insert->values(array(
          'aid' => $aid,
          'weight' => $i++,
          'day' => (string) $day->name,
          'phrase' => accuweather_convert('icon-to-phrase', $day->icon),
          'icon' => (string) $day->icon,
          'temp_high_f' => (float) $day->high,
          'temp_high_c' => accuweather_convert_temperature((float) $day->high),
          'temp_low_f' => (float) $day->low,
          'temp_low_c' => accuweather_convert_temperature((float) $day->low),
        ));
      }

      try {
        $insert->execute();
      }
      catch (Exception $e) {
        var_dump('5DAY');
        var_dump($e);
      }
    }
    watchdog('cron', 'Current 5-day Weather has been rebuilt.');
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_get_metadata($location) {
  static $_cache;

  if (!isset($_cache[$location])) {
    $_cache[$location] = db_select('accuweather', 'a')
            ->fields('a')
            ->condition('a.location', $location, '=')
            ->execute()
            ->fetchAssoc('aid');
  }

  return $_cache[$location];
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_get_aid($location) {
  $metadata = accuweather_get_metadata($location);

  if (!$metadata) {
    return FALSE;
  }

  return $metadata['aid'];
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_get_all_metadata() {
  $results = db_select('accuweather', 'a')
     ->fields('a')
     ->orderBy('weight', 'ASC')
     ->execute();

  $metadata = array();
  foreach ($results as $location) {
    $metadata[$location->aid] = (array) $location;
  }

  return $metadata;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_get_all_current() {
  $results = db_select('accuweather_current', 'ac')
     ->fields('ac')
     ->execute();

  $current = array();
  foreach ($results as $forecast) {
    $current[$forecast->aid] = (array) $forecast;
  }

  return $current;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_get_all_fiveday() {
  $results = db_select('accuweather_five_day', 'af')
    ->fields('af')
    ->orderBy('aid', 'ASC')
    ->orderBy('weight', 'ASC')
    ->execute();

  $fiveday = array();
  $last = NULL;
  foreach ($results as $forecast) {
    if ($last != $forecast->aid) {
      $last = $forecast->aid;

      $fiveday[$last] = array();
    }

    $fiveday[$last][$forecast->day] = (array) $forecast;
  }

  return $fiveday;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function accuweather_get_all_unique_islands_current_and_forecast() {
    $islands = db_select('accuweather', 'a')
                  ->fields('a')
                  ->orderBy('weight', 'ASC')
                  ->groupBy('island')
                  ->having('featured = 1')
                  ->execute()
                  ->fetchAllAssoc('aid');
    
    $aids = array_keys($islands);
    
    $current = db_select('accuweather_current', 'ac')
                   ->fields('ac')
                   ->where('aid in (' . implode(',', $aids) . ')')
                   ->execute()
                   ->fetchAllAssoc('aid');
    
    $fiveday = db_select('accuweather_five_day', 'af')
                  ->fields('af')
                  ->where('aid in (' . implode(',', $aids) . ')')
                  ->where('weight = 0')
                  ->execute()
                  ->fetchAllAssoc('aid');
    
    $ret = array();
    foreach ($islands as $aid => $island) {
        $ret[$aid]['metadata'] = (array)$island;
        $ret[$aid]['current']  = (array)$current[$aid];
        $ret[$aid]['forecast'] = (array)$fiveday[$aid];
    }
    
    return $ret;
}
